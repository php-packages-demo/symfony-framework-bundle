# symfony/framework-bundle

![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/framework-bundle)
Main framework configuration, from sessions and translations to forms, validation, routing and more. https://symfony.com/framework-bundle

## Official Symfony documentation
* [*Create your own PHP Framework*
  ](https://symfony.com/doc/current/create_framework/index.html)
  * [*The Routing Component*
    ](https://symfony.com/doc/current/create_framework/routing.html)
* [*Building your own Framework with the MicroKernelTrait*
  ](https://symfony.com/doc/current/configuration/micro_kernel_trait.html)
  * [*New in Symfony 7.2: Simpler Single-File Symfony Applications*
    ](https://symfony.com/blog/new-in-symfony-7-2-simpler-single-file-symfony-applications)
    2024-11 Javier Eguiluz

# Configuration
* [*Symfony Internals #1: Inside the Framework Configuration*
  ](https://medium.com/the-sensiolabs-tech-blog/symfony-internals-1-inside-the-framework-configuration-8235ec3fecdf)
  2022-03 Alexandre Daubois

# Controllers
* [symfony controller](https://www.google.com/search?q=symfony+controller)
* [*Controller*](https://symfony.com/doc/current/controller.html)
---
* [*A better ADR pattern for your Symfony controllers*
  ](https://www.strangebuzz.com/en/blog/a-better-adr-pattern-for-your-symfony-controllers)
  2024-11 COil (Strangebuzz)
* [*Enhancing Code Decoupling in Symfony with Immutable Data Transfer Objects (DTOs)*
  ](https://itnext.io/enhancing-code-decoupling-in-symfony-with-immutable-data-transfer-objects-dtos-7b4d6d32005f)
  2024-02 Nikolay Nikolov (Medium)
* [*Abstract Controller in Symfony*
  ](https://medium.com/@rcsofttech85/the-benefits-of-avoiding-abstract-controller-in-symfony-a9fe48f4ead8)
  2024-02 rcsofttech85 (Medium)
* [*New in Symfony 5.4: Controller Changes*
  ](https://symfony.com/blog/new-in-symfony-5-4-controller-changes)
  2021-11 Javier Eguiluz
* [*How to retrieve specific and all YAML parameters from services.yaml in Symfony 4*
  ](https://ourcodeworld.com/articles/read/1041/how-to-retrieve-specific-and-all-yaml-parameters-from-services-yaml-in-symfony-4)
  2019-09 Carlos Delgado
* [*How to avoid Fat Controllers in Symfony*
  ](https://medium.com/@_eexbee/how-to-avoid-fat-controllers-in-symfony-294a2f10fc09)
  2019-02 Amin Mosayebi

# Testing
* [*Three simple testing tricks using PHP and Symfony*
  ](https://medium.com/@patrykwozinski/three-simple-testing-tricks-using-php-and-symfony-e4983efad8b5)
  2020-08 Patryk Woziński

## PHPUnit
* [*Disable symfony deprecation warnings in PHPUnit tests*
  ](https://nerdpress.org/2019/08/29/disable-symfony-deprecation-warnings-in-phpunit-tests/)
  2019-08 Ivo Bathke
* [*PHPUnit test listeners and Symfony*
  ](https://medium.com/@zlikavac32/phpunit-test-listeners-and-symfony-8b7987406163)
  2019-08 Marijan Šuflaj
